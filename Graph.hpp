// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph {
public:
    // ------
    // usings
    // ------

    using vertex_descriptor = int;
    using edge_descriptor   = std::pair<vertex_descriptor, vertex_descriptor>;

    using vertex_iterator    = typename std::vector<vertex_descriptor>::const_iterator;
    using edge_iterator      = typename std::set<edge_descriptor>::const_iterator;
    using adjacency_iterator = typename std::set<vertex_descriptor>::const_iterator;

    using vertices_size_type = std::size_t;
    using edges_size_type    = std::size_t;

public:
    // --------
    // add_edge
    // --------

    /**
     * add an edge to graph
     * @param v1 source vertex
     * @param v2 destination vertex
     * @param g graph reference
     * @return pair of edge descriptor and a bool indicates whether the edge has been successfully added
     */
    friend std::pair<edge_descriptor, bool> add_edge (vertex_descriptor v1, vertex_descriptor v2, Graph& g) {

        edge_descriptor ed = std::make_pair(v1, v2);
        bool            b  = false;
        if(g._a.size() > std::max(v1, v2)) {
            // if edge does not exist
            if(g._a[v1].find(v2) == g._a[v1].end()) {
                b = true;
                g._a[v1].insert(v2);
                g._e.insert(ed);
            }
        }
        assert(g.valid());
        return std::make_pair(ed, b);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * add an vertex to graph
     * @param g graph reference
     * @return the vertex descriptor of addded vertex
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        vertex_descriptor v = g._v.size();
        g._v.push_back(v);
        g._a.push_back(std::set<vertex_descriptor>());
        assert(g.valid());
        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * get the iterators of adjacent vertices
     * @param vd the vertex descriptor of vertex to serach for adjacent vertices
     * @param g graph reference
     * @return pari of begin iterator and end iterator of adjacent verticies container
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (vertex_descriptor vd, const Graph& g) {
        if(vd < g._a.size()) {
            return std::make_pair(g._a[vd].begin(), g._a[vd].end());
        }
        std::set<vertex_descriptor> dummy;
        return std::make_pair(dummy.begin(), dummy.end());
    }

    // ----
    // edge
    // ----

    /**
     * get a specific edge
     * @param vd1 source of edge
     * @param vd2 destination of edge
     * @param g graph reference
     * @return pair of edge descriptor and a bool indicates whether the egde exists
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor vd1, vertex_descriptor vd2, const Graph& g) {
        edge_descriptor ed = std::make_pair(vd1, vd2);
        bool            b  = false;
        if(g._a.size() > std::max(vd1, vd2)) {
            b = (g._a[vd1].find(vd2) != g._a[vd1].end());
        }
        return std::make_pair(ed, b);
    }

    // -----
    // edges
    // -----

    /**
     * @param g graph reference
     * @return the pair of begin and end iterator of edge container
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        return std::make_pair(g._e.begin(), g._e.end());
    }

    // ---------
    // num_edges
    // ---------

    /**
     * @param g graph reference
     * @return the number of egdes the graph has
     */
    friend edges_size_type num_edges (const Graph& g) {
        return g._e.size();
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * @param g graph reference
     * @return the number of vertices the graph has
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        return g._a.size();
    }

    // ------
    // source
    // ------

    /**
     * @param ed edge descriptor
     * @param g graph reference
     * @return the vertex descriptor of the source of edge
     */
    friend vertex_descriptor source (edge_descriptor ed, const Graph& g) {
        return ed.first;
    }

    // ------
    // target
    // ------

    /**
     * @param ed edge descriptor
     * @param g graph reference
     * @return the vertex descriptor of the target of edge
     */
    friend vertex_descriptor target (edge_descriptor ed, const Graph& g) {
        return ed.second;
    }

    // ------
    // vertex
    // ------

    /**
     * @param vd index of vertex
     * @param g graph reference
     * @return the vetex descriptor of vdth vertex in g
     */
    friend vertex_descriptor vertex (vertices_size_type vd, const Graph& g) {
        return vd;
    }

    // --------
    // vertices
    // --------

    /**
     * @param g graph reference
     * @return pair of begin and end itertaor of vertex conatiner
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        return std::make_pair(g._v.begin(), g._v.end());
    }

private:
    // ----
    // data
    // ----

    std::vector<std::set<vertex_descriptor>> _a;
    std::set<edge_descriptor>                _e;
    std::vector<vertex_descriptor>           _v;


    // -----
    // valid
    // -----

    /**
     * check valid
     */
    bool valid () const {
        return _a.size() == _v.size();
    }

public:
    // --------
    // defaults
    // --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

#endif // Graph_h
